import pandas as pd

df = pd.read_json("static/json/data.json")

def get_group(mail):
    return df[df.Correo == mail].Grupo.values[0]

def get_group_members(mail):
    group_id = get_group(mail)
    aux = df[df.Grupo == group_id]
    group_members = [f"{name} {surname}" for name, surname in zip(aux.Nombre, aux.Apellido)]
    return group_members, group_id

# print(get_group_members("alurregoa@unal.edu.co"))