from flask import Flask, request, redirect, render_template, session, send_file
from flask_session import Session
import search_json as search

app = Flask(__name__)
SESSION_TYPE = "filesystem"
app.config.from_object(__name__)
Session(app)

@app.route("/", methods = ["GET", "POST"])
def index():
    if "email" not in session:
        session["email"] = ""


    if request.method == "POST":
        try:
            session["email"] = f"{request.form['user']}@unal.edu.co"
            group_members, group_id = search.get_group_members(session["email"])
            return render_template("groups.html", group_members = group_members, group_id = group_id)
        except Exception:
            return render_template("groups.html", group_members = [], group_id = "")
    
    return render_template("groups.html", group_members = [], group_id = "")
